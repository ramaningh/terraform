variable "vpc_id" {
  type        = string
  description = "providing vpc id"
}

variable "rt_cidr_block" {
  type        = string
  description = "providing cidr block for route table"
}

variable "gateway_id" {
  type        = string
  description = "providing internet gateway id"
}

variable "public_route_table_tag" {
  type        = string
  description = "providing tag for public route table"
}

variable "pub_subnet_id" {
  type        = string
  description = "providing public subnet id"
}

variable "nat_id" {
  type        = string
  description = "providing nat gateway id"
}
variable "private_rt_cidr_block" {
    description = "specify private route table cidr here"
    type = string
}

variable "private_route_table_tag" {
  type = string
  description = "tag for private route table"
}

variable "private_subnet_id" {
  type      = list(string)
  description = "providing private subnet id"
}