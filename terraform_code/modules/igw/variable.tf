variable "vpc_id" {
  default     = ""
  description = "providing vpc id"
}

variable "igw_tag" {
  default     = ""
  description = "tag for internet gateway"
}