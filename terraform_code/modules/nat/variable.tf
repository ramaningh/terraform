variable "eip_tag" {
  type        = string
  description = "providing elastic ip tag"
}

variable "subnet_id" {
  type        = string
  description = "providing subnet id"
}

variable "nat_tag" {
  type        = string
  description = "tag for internet gateway"
}