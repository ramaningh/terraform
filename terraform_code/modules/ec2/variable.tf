variable "pub_instance_type" {
  type = string
  description = "public instance"
}

variable "pub_subnet_id" {
  type = string
  description = "specify public subnet id here"
}

variable "key_name" {
  type = string
  description = "specify key name here"
}

variable "pub_instance_tag" {
  type = string
  description = "specify pub_instance tag"
}

variable "private_instance_type" {
  type = list(string)
  description = "private instance type"
}

variable "private_subnet_id" {
  type = list(string)
  description = "specify private subnet id here"
}

variable "private_instance_tag" {
  type = list(string)
  description = "specify private_instance tag"
}

variable "public_security_group_id" {
  type = string
  description = "specify security group id"
}
variable "private_security_group_id" {
  type = string
  description = "specify security group id"
}