data "aws_ami" "ubuntu" {
    most_recent = true
    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }
    filter {
        name   = "architecture"
        values = ["x86_64"]
    }
    owners = ["099720109477"] 
}
resource "aws_instance" "ec2_bastion" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.pub_instance_type
  subnet_id     = var.pub_subnet_id
  key_name      = var.key_name
  vpc_security_group_ids = [var.public_security_group_id]
  
  tags = {
    Name = var.pub_instance_tag
  }
}

resource "aws_instance" "ec2_private" {
  count         = length(var.private_instance_type)
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.private_instance_type[count.index]
  subnet_id     = var.private_subnet_id[count.index]
  key_name      = var.key_name
  vpc_security_group_ids = [var.private_security_group_id]

  tags = {
    Name = var.private_instance_tag[count.index]
  }
}
