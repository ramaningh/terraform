variable "vpc_cidr_block" {
  default     = ""
  description = "creating a vpc"
}

variable "vpc_tag" {
  default     = ""
  description = "assigning tag to vpc"
}