variable "vpc_id" {
  type = string
  description = "providing vpc id"
}

variable "pub_subnet_cidr_block" {
  type = string
  description = "specify a cidr range"
}

variable "av_zone_pub" {
  type = string
  description = "description"
}

variable "pub_subnet_tag" {
  type = string
  description = "providing tag for public subnet"
}

variable "private_subnet_cidr_block" {
  type = list(string)
  description = "specify a cidr range for private subnet"
}

variable "av_zone_private" {
  type = list(string)
  description = "description"
}

variable "private_subnet_tag" {
  type = list(string)
  description = "providing tag for private subnet"
}

variable "map_public_ip_on_launch" {
  type = bool
  description = ""
}