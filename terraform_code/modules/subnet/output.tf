output "pub_subnet_id" {
    value = aws_subnet.subnet_public.id
}

output "private_subnet_id" {
    value = aws_subnet.subnet_private[*].id
}