resource "aws_subnet" "subnet_public" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.pub_subnet_cidr_block
  map_public_ip_on_launch = true
  availability_zone       = var.av_zone_pub

  tags = {
    Name = var.pub_subnet_tag
  }
}

resource "aws_subnet" "subnet_private" {
  count             = length(var.private_subnet_cidr_block)
  vpc_id            = var.vpc_id
  cidr_block        = var.private_subnet_cidr_block[count.index]
  map_public_ip_on_launch = var.map_public_ip_on_launch
  availability_zone = var.av_zone_private[count.index]

  tags = {
    Name = var.private_subnet_tag[count.index]
  }
}