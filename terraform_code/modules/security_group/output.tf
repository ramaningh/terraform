output "public_sg_id" {
    value = aws_security_group.sg_public.id
}
output "private_sg_id" {
    value = aws_security_group.sg_private.id
}