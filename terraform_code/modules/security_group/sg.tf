 resource "aws_security_group" "sg_public" {
  vpc_id      = var.vpc_id
  name        = var.public_sg_name
  dynamic "ingress" {
    for_each = var.inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.public_sg_protocol
      cidr_blocks = var.public_ingress_cidr_block
    }
  }
   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "sg_private" {
  vpc_id      = var.vpc_id
  name        = var.private_sg_name
  dynamic "ingress" {
    for_each = var.private_inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.private_sg_protocol
      cidr_blocks = var.private_ingress_cidr_block
    }
  }
   egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}













