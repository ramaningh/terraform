# variable "vpc_id" {
#   type = string
#   description = "providing vpc id"
# }

# variable "sg_tag" {
#   type     = list(string)
#   description = "specify tag for security group"
# }

# variable "sg_cidr_block" {
#   type     = list(string)
#   description = "specify cidr for security group"
# }   

variable "vpc_id" {
   description = "specify the vpc id here"
   type = string
}
variable "public_sg_name" {
   description = "specify the security group name here"
   type = string
}
variable "inbound_ports" {
   description = "specify the inbound ports here"
   type = list(string)
}
variable "public_sg_protocol" {
   description = "specify the ingress protocol here"
   type = string
}
variable "public_ingress_cidr_block" {
   description = "specify the ingress cidr block here"
   type = list(string)
}

variable "private_sg_name" {
   description = "specify the security group name here"
   type = string
}
variable "private_inbound_ports" {
   description = "specify the inbound ports here"
   type = list(string)
}
variable "private_sg_protocol" {
   description = "specify the ingress protocol here"
   type = string
}
variable "private_ingress_cidr_block" {
   description = "specify the ingress cidr block here"
   type = list(string)
}






