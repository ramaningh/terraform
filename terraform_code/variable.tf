variable "vpc_cidr_block" {
  type        = string
  description = "specify the vpc id here"
}

variable "vpc_tag" {
  type        = string
  description = "specify the vpc tag here"
}

variable "pub_subnet_cidr_block" {
  type        = string
  description = "specify the public subnet cidr here"
}

variable "av_zone_pub" {
  type        = string
  description = "specify the public az here"
}

variable "pub_subnet_tag" {
  type        = string
  description = "specify the public subnat tag here"
}

variable "private_subnet_cidr_block" {
  type        = list(string)
  description = "specify the private subnet cidr here"
}

variable "av_zone_private" {
  type        = list(string)
  description = "specify the private az here"
}

variable "private_subnet_tag" {
  type        = list(string)
  description = "specify the private subnet tag here"
}

variable "pub_instance_type" {
  type        = string
  description = "specify public instance type here"
}

variable "key_name" {
  type        = string
  description = "specify key name here"
}

variable "pub_instance_tag" {
  type        = string
  description = "specify public instance tag here"
}

variable "private_instance_type" {
  type        = list(string)
  description = "specify private instance type here"
}

variable "private_instance_tag" {
  type        = list(string)
  description = "specify public instance tag here"
}


variable "eip_tag" {
  type        = string
  description = "specify elastic ip tag here"
}

variable "nat_tag" {
  type        = string
  description = "specify nat gateway tag here"
}

variable "igw_tag" {
  type        = string
  description = "specify internet gateway tag here"
}

# variable "protocol" {
#   type        = string
#   description = "specify number"
# }

# variable "rule_no" {
#   type        = number
#   description = "specify number"
# }

# variable "action" {
#   default     = ""
#   description = "description"
# }

# variable "nacl_cidr_block" {
#   default     = ""
#   description = "description"
# }

# variable "from_port" {
#   type        = number
#   description = "specify port"
# }

# variable "to_port" {
#   type        = number
#   description = "specify port"
# }

# variable "nacl_tag" {
#   type        = string
#   description = "specify tag for nacl"
# }

variable "rt_cidr_block" {
  type        = string
  description = "specify public route table cidr here"
}

variable "public_route_table_tag" {
  type        = string
  description = "specify public route table tag here"
}
variable "private_rt_cidr_block" {
  type        = string
  description = "specify private route table cidr here"
}


variable "private_route_table_tag" {
  type        = string
  description = "specify private route table tag here"
}
variable "map_public_ip_on_launch" {
  type        = bool
  description = "providing ip to public instance"
}
variable "public_sg_name" {
  description = "specify the security group name here"
  type        = string
}
variable "inbound_ports" {
  description = "specify the inbound ports here"
  type        = list(string)
}
variable "public_sg_protocol" {
  description = "specify the ingress protocol here"
  type        = string
}
variable "public_ingress_cidr_block" {
  description = "specify the ingress cidr block here"
  type        = list(string)
}

variable "private_sg_name" {
  description = "specify the security group name here"
  type        = string
}
variable "private_inbound_ports" {
  description = "specify the inbound ports here"
  type        = list(string)
}
variable "private_sg_protocol" {
  description = "specify the ingress protocol here"
  type        = string
}
variable "private_ingress_cidr_block" {
  description = "specify the ingress cidr block here"
  type        = list(string)
}
