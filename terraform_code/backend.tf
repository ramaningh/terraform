terraform {
  backend "s3" {
    bucket  = "elasticsearchterraformcode"
    key     = "oregon.pem/terraform.tfstate"
    region  = "us-west-2"
    encrypt = true
  }
}
