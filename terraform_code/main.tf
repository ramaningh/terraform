module "vpc" {
  source         = "./modules/vpc"
  vpc_cidr_block = var.vpc_cidr_block
  vpc_tag        = var.vpc_tag
}

module "subnet" {
  source                    = "./modules/subnet"
  vpc_id                    = module.vpc.vpc_id
  pub_subnet_cidr_block     = var.pub_subnet_cidr_block
  av_zone_pub               = var.av_zone_pub
  pub_subnet_tag            = var.pub_subnet_tag
  private_subnet_cidr_block = var.private_subnet_cidr_block
  av_zone_private           = var.av_zone_private
  private_subnet_tag        = var.private_subnet_tag
  map_public_ip_on_launch   = var.map_public_ip_on_launch
}

module "ec2" {
  source                    = "./modules/ec2"
  pub_instance_type         = var.pub_instance_type
  pub_subnet_id             = module.subnet.pub_subnet_id
  key_name                  = var.key_name
  pub_instance_tag          = var.pub_instance_tag
  private_instance_type     = var.private_instance_type
  private_subnet_id         = module.subnet.private_subnet_id[*]
  private_instance_tag      = var.private_instance_tag
  public_security_group_id  = module.sg.public_sg_id
  private_security_group_id = module.sg.private_sg_id

}

module "sg" {
  source                     = "./modules/security_group"
  vpc_id                     = module.vpc.vpc_id
  public_sg_name             = var.public_sg_name
  inbound_ports              = var.inbound_ports
  public_sg_protocol         = var.public_sg_protocol
  public_ingress_cidr_block  = var.public_ingress_cidr_block
  private_sg_name            = var.private_sg_name
  private_inbound_ports      = var.private_inbound_ports
  private_sg_protocol        = var.private_sg_protocol
  private_ingress_cidr_block = var.private_ingress_cidr_block
}

module "igw" {
  source  = "./modules/igw"
  vpc_id  = module.vpc.vpc_id
  igw_tag = var.igw_tag
}

module "nat" {
  source    = "./modules/nat"
  eip_tag   = var.eip_tag
  subnet_id = module.subnet.pub_subnet_id
  nat_tag   = var.nat_tag
}

# module "nacl" {
#   source          = "./modules/nacl"
#   vpc_id          = module.vpc.vpc_id
#   protocol        = var.protocol
#   rule_no         = var.rule_no
#   action          = var.action
#   nacl_cidr_block = var.nacl_cidr_block
#   from_port       = var.from_port
#   to_port         = var.to_port
#   nacl_tag        = var.nacl_tag
#   subnet_id       = module.subnet.private_subnet_id[*]
# }

module "route_table" {
  source                  = "./modules/route_table"
  vpc_id                  = module.vpc.vpc_id
  rt_cidr_block           = var.rt_cidr_block
  gateway_id              = module.igw.igw_id
  public_route_table_tag  = var.public_route_table_tag
  pub_subnet_id           = module.subnet.pub_subnet_id
  nat_id                  = module.nat.nat_id
  private_rt_cidr_block   = var.private_rt_cidr_block
  private_route_table_tag = var.private_route_table_tag
  private_subnet_id       = module.subnet.private_subnet_id[*]
}
